# microbit-dockerfiles
These are docker images that allow you to very easily compile different firmware for the bbc micro:bit.

Compile Instructions:

* micropython: `docker run --rm -v /host/path/for/output:/mounted_folder bfayers/microbit-dockerfiles:micropython`

* espruino: `docker run --rm -v /host/path/for/output:/mounted_folder bfayers/microbit-dockerfiles:espruino`

WIP images:

* [RIOT-OS](https://github.com/RIOT-OS/RIOT)

* [RUST](https://github.com/SimonSapin/rust-on-bbc-microbit)

* [PXT_repo1](https://github.com/microsoft/pxt) [PXT_repo2](https://github.com/microsoft/pxt-microbit)